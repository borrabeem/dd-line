# Lambda function
## Step 1: Create lambda function
![](https://www.img.in.th/images/e3cb8c15761387fa455c7359241d21df.png)

### 1. Select language
เลือกภาษาที่ใช้สำหรับโปรเจค
![](https://www.img.in.th/images/8a506af46a7fa853488b66da294355d5.png)

### 2. Select enable function URL 
ติ๊ก enable function URL  จะได้มีลิ้งไว้สำหรับเช็คข้อมูล
![](https://www.img.in.th/images/c4b59182046c9eb9636c5940f9942503.png)

หลังจากสร้างเสร็จะได้หน้านี้ออกมา
![](https://www.img.in.th/images/eafbfcc53fa9c3860fa2d68488fe71d0.png)

## Step 2: Add trigger to function

### 1. Select API Gateway
![](https://www.img.in.th/images/8f3046cd57a2626fa58f6751cf6cfcdf.png)

### 2. Use existing API and select api
เลือก API ที่จะใช้หลังจากนั้นเลือก Deployment stage และ security เสร็จแล้ว Add 
![](https://www.img.in.th/images/42760f7b9f43e94adbb02a08b9edbcd6.png)

จะเห็นว่ามี API Gateway ขึ้นมาที่ function 
![](https://www.img.in.th/images/0d68058755308ede9c5e58fcbb9695f5.png)

## Step 3: Coding
```
package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"reflect"

	ddlambda "github.com/DataDog/datadog-lambda-go"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	ginadapter "github.com/awslabs/aws-lambda-go-api-proxy/gin"
	"github.com/gin-gonic/gin"
	"github.com/juunini/simple-go-line-notify/notify"
	httptrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/net/http"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
)

type Message struct {
	Event_title string
	Alert_title string
	Tags        string
	Priority    string
	Link        string
}

var ginLambda *ginadapter.GinLambda

func lambdaHandler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	if ginLambda == nil {
		ginLambda = ginadapter.New(ginEngine())
	}

	// Trace an HTTP request
	requ, _ := http.NewRequestWithContext(ctx, "GET", "https://www.datadoghq.com", nil)
	client := http.Client{}
	client = *httptrace.WrapClient(&client)
	client.Do(requ)

	ddlambda.Metric(
		"dd-notify",     // Metric name
		10,              // Metric value
		"notify-golang", // Associated tags
	)

	// Connect your Lambda logs and traces
	currentSpan, _ := tracer.SpanFromContext(ctx)
	log.Printf("my log message %v", currentSpan)

	// Create a custom span
	s, _ := tracer.StartSpanFromContext(ctx, "notify-golang")
	s.Finish()

	return ginLambda.ProxyWithContext(ctx, req)

}

func ginEngine() *gin.Engine {
	app := gin.Default()

	app.POST("/alert", func(c *gin.Context) {

		// Create a span for a web request at the /posts URL.
		span := tracer.StartSpan("web.request", tracer.ResourceName("/posts"))
		defer span.Finish()
		var message Message
		// json.Unmarshal([]byte(messageJson), &message)

		if err := c.ShouldBindJSON(&message); err != nil {
			c.JSON(400, gin.H{"message": err.Error()})
			return
		}
		fmt.Println(message)
		fmt.Println("message = ", reflect.TypeOf(message).Kind())

		accessToken := "hHQsMJBsdNm5rYptpoiqtoCv86CFKnEq46hzlsBzudb"
		NotifyMessage := "Event_title = " + message.Event_title + "\n" + "Alert_title = " + message.Alert_title + "\n" + "tags = " + message.Tags + "\n" + "priority = " + message.Priority + "\n" + "link = " + message.Link + "\n" + ""

		if err := notify.SendText(accessToken, NotifyMessage); err != nil {
			panic(err)
		}

		c.JSON(200, message)
	})

	return app
}

func main() {
	tracer.Start(tracer.WithService("DD-notify-golang"))
	if gin.Mode() == "release" {
		lambda.Start(ddlambda.WrapFunction(lambdaHandler, nil))
	} else {
		app := ginEngine()
		app.Run(":3000")
	}
}

```
เขียนโค้ดและอัพโหลดขึ้นไปบนaws จะสามารถใช้งานได้เลย
![](https://www.img.in.th/images/8b2c9bd64ce74655154f1504d177db7e.png)

# Add lambda function to gitlab CI/CD pipeline

## Step 1: Create AWS access key
ไปที่แอคเค้าท์ตัวเองด้านขวาบน เลือก Security credentials
![](https://sv1.picz.in.th/images/2022/09/06/ajPjtS.png)

หลังจากนั้นกด Create access key
![](https://sv1.picz.in.th/images/2022/09/06/ajsuXl.png)

จะได้ access key ขึ้นมา ให้ดาวน์โหลดเก็บไว้
![](https://sv1.picz.in.th/images/2022/09/06/aj2YOa.png)

## Step 2:  Put aws access key to gitlab ci/cd variable
ไปที่ gitlab project แล้วเลือก setting แถบ CI/CD หัวข้อ Variables
![](https://sv1.picz.in.th/images/2022/09/07/ajFPEq.png)


Add variables AWS_ACCESS_KEY_ID, AWS_DEFAULT_REGION, AWS_SECRET_ACCESS_KEY
![](https://sv1.picz.in.th/images/2022/09/07/ajmLpl.png)

## Step 3:  Add golang file to gitlab repository 
![](https://sv1.picz.in.th/images/2022/09/07/ajFakt.png)

## Step 4:  Create .gitlab-ci.yml  file
![](https://sv1.picz.in.th/images/2022/09/07/ajFakt.png)

In .gitlab-ci.yml  file  create 2 stage 
First stage is use to install, update, build and zip file
Second stage is use to push code to aws lambda function that had created.

```
stages:
    - build
    - deploy

build:
    stage: build
    tags: 
    - ci
    image: ubuntu
    when:
        on_success
    before_script:
        - apt-get update
        - apt-get install zip -y
        - apt search golang-go
        - apt search gccgo-go
        - apt install golang-go -y
        - apt-get install ca-certificates -y
    script:
        - GOOS=linux CGO_ENABLED=0 go build -o main main.go
        - zip function.zip main
    artifacts:
        paths:
        - function.zip
        


deploy:
    stage: deploy
    tags: 
    - ci
    image:
        name: amazon/aws-cli
        entrypoint: [""]
    script:
        - aws --version
        - aws configure set region ap-southeast-1
        - aws lambda update-function-code --function-name dd-notify-golang --zip-file fileb://function.zip

```
## Step 4:  Check in gitlab pipeline and aws lambda
หลังจากนนี้จะสามารถ update โค้ดโดยไม่ต้องเข้าไปที่ aws ได้เลย
![](https://sv1.picz.in.th/images/2022/09/07/ajFpEe.png)

# Add enchanced metrics to datadog

## Step 1: Go to integration in Datadog
Amazon Web Services Integration
![](https://sv1.picz.in.th/images/2022/09/07/ajFJzk.png)

## Step 2: Create account 
Select Infrastructure Monitoring, Log Management, Serverless Monitoring, Cloud Security Posture Management.

Choose Region and API Key ถ้าไม่มี API  Key สามารถสร้างใหม่ได้เลย
![](https://sv1.picz.in.th/images/2022/09/07/ajFLRv.png)
หลังจากนั้นจะได้ aws_account มาให้ enable Metric Collection 
![](https://imgz.io/images/2022/09/07/Screenshot-2022-09-07-040135.png)
ติ๊กที่ Standard collection, Expamded collection required for Cloud Security Posture Management, AWS Lambda and AWS AppSync หลังจากนั้นให้ update configuration ที่ด้านซ้ายล่าง
![](https://imgz.io/images/2022/09/07/Screenshot-2022-09-07-040432.png)

## Step 3: Add trace to golang file
```
tracer.Start(tracer.WithService("DD-notify-golang"))
```

### วิธีเช็คว่าทำงานได้ไหม
ให้ไปที่หน้า Infrastructure หัวข้อ Serverless
![](https://imgz.io/images/2022/09/07/Screenshot-2022-09-07-041020.png)
หลังจากนั้นหาชื่อfunction ที่ใช้จะเห็นว่ามีติ๊กถูกสีเขียวที่ enchanced metrics ของ functionนั้น
โดยปกติจะใช้เวลาประมาณ 30 นาที ถึงจะขึ้นในdatadog
![](https://imgz.io/images/2022/09/07/Screenshot-2022-09-07-041605.png)



