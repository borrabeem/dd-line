package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"reflect"

	ddlambda "github.com/DataDog/datadog-lambda-go"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	ginadapter "github.com/awslabs/aws-lambda-go-api-proxy/gin"
	"github.com/gin-gonic/gin"
	"github.com/juunini/simple-go-line-notify/notify"
	httptrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/net/http"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
)

type Message struct {
	Event_title string
	Alert_title string
	Tags        string
	Priority    string
	Link        string
}

var ginLambda *ginadapter.GinLambda

func lambdaHandler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	if ginLambda == nil {
		ginLambda = ginadapter.New(ginEngine())
	}

	// Trace an HTTP request
	requ, _ := http.NewRequestWithContext(ctx, "GET", "https://www.datadoghq.com", nil)
	client := http.Client{}
	client = *httptrace.WrapClient(&client)
	client.Do(requ)

	ddlambda.Metric(
		"dd-notify",     // Metric name
		10,              // Metric value
		"notify-golang", // Associated tags
	)

	// Connect your Lambda logs and traces
	currentSpan, _ := tracer.SpanFromContext(ctx)
	log.Printf("my log message %v", currentSpan)

	// Create a custom span
	s, _ := tracer.StartSpanFromContext(ctx, "notify-golang")
	s.Finish()

	return ginLambda.ProxyWithContext(ctx, req)

}

func ginEngine() *gin.Engine {
	app := gin.Default()

	app.POST("/alert", func(c *gin.Context) {

		// Create a span for a web request at the /posts URL.
		span := tracer.StartSpan("web.request", tracer.ResourceName("/posts"))
		defer span.Finish()
		var message Message
		// json.Unmarshal([]byte(messageJson), &message)

		if err := c.ShouldBindJSON(&message); err != nil {
			c.JSON(400, gin.H{"message": err.Error()})
			return
		}
		fmt.Println(message)
		fmt.Println("message = ", reflect.TypeOf(message).Kind())

		accessToken := "hHQsMJBsdNm5rYptpoiqtoCv86CFKnEq46hzlsBzudb"
		NotifyMessage := "Event_title = " + message.Event_title + "\n" + "Alert_title = " + message.Alert_title + "\n" + "tags = " + message.Tags + "\n" + "priority = " + message.Priority + "\n" + "link = " + message.Link + "\n" + ""

		if err := notify.SendText(accessToken, NotifyMessage); err != nil {
			panic(err)
		}

		c.JSON(200, message)
	})

	return app
}

func main() {
	tracer.Start(tracer.WithService("DD-notify-golang"))
	if gin.Mode() == "release" {
		lambda.Start(ddlambda.WrapFunction(lambdaHandler, nil))
	} else {
		app := ginEngine()
		app.Run(":3000")
	}
}
